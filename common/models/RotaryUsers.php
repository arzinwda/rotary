<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rotary_users".
 *````
 * @property int $id
 * @property string $name
 * @property string $profile_link
 * @property string|null $image
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $address
 * @property string|null $fb_profile
 */
class RotaryUsers extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'profile_link'], 'required'],
            [['name', 'profile_link', 'image', 'email', 'phone', 'address', 'fb_profile'], 'trim'],
            ['email', 'email'],
            [['profile_link', 'fb_profile'], 'url'],
            [['name', 'profile_link', 'image', 'email', 'phone', 'address', 'fb_profile'], 'string', 'max' => 255]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'profile_link' => Yii::t('app', 'Profile Link'),
            'image' => Yii::t('app', 'Image'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'address' => Yii::t('app', 'Address'),
            'fb_profile' => Yii::t('app', 'FB profile'),
        ];
    }
}
