<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%admin}}', [
            'id' => $this->primaryKey()->notNull()->unsigned(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull(),
            'avatar' => $this->string(),
            'created' => $this->integer()->unsigned(),
            'updated' => $this->integer()->unsigned()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%admin}}');
    }
}
