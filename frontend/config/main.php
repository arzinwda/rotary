<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-rotary-users',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'admin/index',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-rotary-users',
        ],
        'user' => [
            'identityClass' => 'frontend\models\Admin',
            'enableAutoLogin' => false,
            'loginUrl' => ['admin/login']
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-rotary-users',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'admin/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login' => 'admin/login',
                'logout' => 'admin/logout',
                'profile' => 'admin/profile',
                'change-password' => 'admin/change-password',
                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>'
            ],
        ],
    ],
    'params' => $params,
];
