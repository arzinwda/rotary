<?php

use common\widgets\Alert;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RotaryUsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Rotary Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Instructors List</h3>
                </div>
                <?php Alert::builder() ?>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterPosition' => GridView::FILTER_POS_HEADER,
                        'filterModel' => $searchModel,
                        'options' => ['class' => 'table-responsive'],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'Image',
                                'format' => 'html',
                                'value' => function ($model) {
                                    return Html::img($model->image, ['class' => 'direct-chat-img']);
                                },
                            ],
                            [
                                'attribute' => 'name',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a($model->name, Url::to($model->profile_link), ['target' => '_blank']);
                                },
                            ],
                            [
                                'attribute' => 'email',
                                'format' => 'email',
                                'value' => function ($model) {
                                    if ($model->email == 'undefined') {
                                        return null;
                                    }
                                    return $model->email;
                                },
                            ],
                            [
                                'attribute' => 'phone',
                                'value' => function ($model) {
                                    if ($model->phone == 'undefined') {
                                        return null;
                                    }

                                    return $model->phone;
                                },
                            ],
                            [
                                'attribute' => 'address',
                                'value' => function ($model) {
                                    if ($model->address == 'undefined' || !$model->address) {
                                        return null;
                                    }

                                    return $model->address;
                                },
                            ],
                            [
                                'attribute' => 'profile_link',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a($model->profile_link, Url::to($model->profile_link), ['target' => '_blank']);
                                },
                            ],
                            [
                                'attribute' => 'fb_profile',
                                'format' => 'raw',
                                'contentOptions' => ['class' => 'text-center'],
                                'value' => function ($model) {
                                    if (!$model->fb_profile) {
                                        return null;
                                    }
                                    return Html::a('<i class="fa fa-facebook"></i>', Url::to($model->fb_profile), ['target' => '_blank']);
                                },
                            ],
                            [
                                'attribute' => 'fb search',
                                'format' => 'raw',
                                'contentOptions' => ['class' => 'text-center'],
                                'value' => function ($model) {
                                    return Html::a('<i class="fa fa-search"></i>', Url::to(sprintf("https://www.facebook.com/search/people/?q=%s&epa=SERP_TAB", $model->name)), ['target' => '_blank']);
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['class' => 'action-column text-center', 'style' => 'min-width: 163px'],
                                'header' => 'Actions',
                                'template' => '{update} {delete}',
                                'buttons' => [
                                    'update' => function ($url, $model) {
                                        return Html::a('View and Update', $url, [
                                            'class' => 'btn btn-warning btn-xs'
                                        ]);
                                    },
                                    'delete' => function ($url, $model) {
                                        return Html::a('Delete', $url, [
                                            'data-confirm' => "Are you sure you want to delete?",
                                            'class' => 'btn btn-danger btn-xs'
                                        ]);
                                    }
                                ]
                            ]
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

























































