<?php

use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RotaryUsers */

$this->title = Yii::t('app', 'New User');
?>

<div class="row">
    <!-- left column -->
    <div class="col-md-6 col-md-offset-3">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><?= $this->title ?></h3>
            </div>
            <?php Alert::builder() ?>
            <div class="box-body">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'profile_link')->textInput() ?>
                <?= $form->field($model, 'email')->textInput() ?>
                <?= $form->field($model, 'phone')->textInput() ?>
                <?= $form->field($model, 'address')->textInput() ?>
                <?= $form->field($model, 'fb_profile')->textInput() ?>
                <?= $form->field($model, 'image')->textInput() ?>
                <div class="form-group">
                    <img src="<?= $model->image ?>"
                         alt=""
                         class='img-responsive'>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('<i class="fa fa-plus-circle"></i> Create', ['class' => 'btn btn-info btn-block btn-flat']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (left) -->
</div>