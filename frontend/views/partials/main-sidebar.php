<?php

use common\models\Contact;
use yii\widgets\Menu;

?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?php
        try {
            echo Menu::widget([
                'items' => [
                    ['label' => '', 'options' => ['class' => 'header']],
                    // Instructor Block
                    ['label' => sprintf('<i class="fa fa-users" aria-hidden="true"></i> <span> %s </span>  <i class="fa fa-angle-left pull-right"></i>', 'Users'),
                        'url' => ['#'],
                        'template' => '<a href="{url}" >{label}<i class="fa fa-angle-left pull-right"></i></a>',
                        'items' => [
                            ['label' => sprintf('<i class="fa fa-plus-circle"></i> %s', 'New user'), 'url' => ['rotary-users/create']],
                            ['label' => sprintf('<i class="fa fa-table"></i> %s', 'List'), 'url' => ['rotary-users/index']],
                        ],
                        'options' => ['class' => 'treeview'],
                    ],
                ],
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'submenuTemplate' => "<ul class='treeview-menu'>{items}</ul>",
                'encodeLabels' => false,
                'activateParents' => true,
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'app');
        }
        ?>
    </section>
</aside>
