<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RotaryUsers;

class RotaryUsersSearch extends RotaryUsers
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'rotary_users';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'profile_link', 'image', 'email', 'phone', 'address', 'fb_profile'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RotaryUsers::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'profile_link', $this->profile_link])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'fb_profile', $this->fb_profile]);

        return $dataProvider;
    }
}
