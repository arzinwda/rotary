<?php

namespace frontend\controllers;

use frontend\models\Admin;
use frontend\models\ChangePassword;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ServerErrorHttpException;

class AdminController extends Controller
{
    public $layout = 'main';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['logout', 'error', 'index', 'profile', 'change-password'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@', '?']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }

        $this->layout = 'main';
        return $this->redirect(['rotary-users/index']);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'blank';

        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['rotary-users/index']);
        }

        $model = new Admin();
        $model->setScenario(Admin::SCENARIO_LOGIN);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /**@var $admin Admin */
            $admin = Admin::findByEmail($model->email);

            if (!is_null($admin) && (Yii::$app->getSecurity()->validatePassword(md5($model->password), $admin->password))) {
                try {
                    if (Yii::$app->user->login($admin, 3600)) {
                        return $this->redirect(['rotary-users/index']);
                    }
                } catch (\Exception $e) {
                    Yii::error($e->getMessage(), 'app');
                }
            }

            Yii::$app->getSession()->setFlash('error', [
                'type' => 'error',
                'duration' => 3000,
                'icon' => 'fa fa-info',
                'message' => Yii::t('app', 'Invalid login or password'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionProfile()
    {
        $this->layout = 'main';

        /* @var $model Admin */
        $model = Admin::findOne(Yii::$app->user->identity->getId());
        $model->setScenario(Admin::SCENARIO_UPDATE);

        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
//                print_r($model->save());die;
                Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                    'duration' => 3000,
                    'icon' => 'fa fa-success',
                    'message' => Yii::t('app', 'Profile updated success'),
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);
                return $this->redirect(['profile']);
            }
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'app');

            Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                'duration' => 3000,
                'icon' => 'fa fa-danger',
                'message' => Yii::t('app', 'Profile don\'t updated'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        }

        if ($model->hasErrors()) {
            throw new ServerErrorHttpException();
        }

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            /* @var $admin Admin */
            $admin = Admin::findOne(Yii::$app->user->id);;
            $admin->password = Yii::$app->security->generatePasswordHash(md5($model->password));

            try {
                if ($admin->save(false)) {

                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 3000,
                        'icon' => 'fa fa-success',
                        'message' => Yii::t('app', 'Password changed success'),
                        'positonY' => 'top',
                        'positonX' => 'center'
                    ]);

                    return $this->redirect(['change-password']);
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), 'app');
                Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                    'duration' => 3000,
                    'icon' => 'fa fa-danger',
                    'message' => Yii::t('app', 'An error has occurred'),
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);
            }
        }

        if ($model->hasErrors()) {
            throw new ServerErrorHttpException();
        }

        return $this->render('change-password', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionError()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = 'main';
        }

        return $this->render('error');
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout(true);

        return $this->redirect(['login']);
    }
}
