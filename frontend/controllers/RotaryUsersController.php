<?php

namespace frontend\controllers;

use frontend\models\RotaryUsersSearch;
use Yii;
use common\models\RotaryUsers;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RotaryUsersController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new RotaryUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new RotaryUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RotaryUsers();

        try {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                    'duration' => 2000,
                    'icon' => 'fa fa-success',
                    'message' => Yii::t('app', 'User created successfully.'),
                    'positonY' => 'top',
                    'positonX' => 'center'
                ]);

                return $this->redirect(['rotary-users/index']);
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-danger',
                'message' => Yii::t('app', 'Something went wrong. Please try again!'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing RotaryUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        try {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 2000,
                        'icon' => 'fa fa-success',
                        'message' => Yii::t('app', 'User updated successfully.'),
                        'positonY' => 'top',
                        'positonX' => 'center'
                    ]);

                    return $this->goHome();
                }
            }
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-danger',
                'message' => Yii::t('app', 'Something went wrong. Please try again!'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        try {
            $model->delete();
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                'duration' => 2000,
                'icon' => 'fa fa-success',
                'message' => Yii::t('app', 'User deleted successfully.'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        } catch (\Exception $e) {
            Yii::$app->getSession()->setFlash('error', [
                'type' => 'danger',
                'duration' => 3000,
                'icon' => 'fa fa-danger',
                'message' => Yii::t('app', 'Something went wrong. Please try again!'),
                'positonY' => 'top',
                'positonX' => 'center'
            ]);
        }

        return $this->redirect('index');
    }

    /**
     * Finds the RotaryUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return RotaryUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RotaryUsers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

